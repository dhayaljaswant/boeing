import React from "react";

const Box = ({ boxColor, boxNumber, handleDrag, handleDrop, children }) => {
  return (
    <div
      draggable={true}
      id={boxNumber}
      onDragOver={(ev) => ev.preventDefault()}
      onDragStart={handleDrag}
      onDrop={handleDrop}
      style={{
        backgroundColor: boxColor,
        border: "1px solid",
        borderColor: boxColor,
        borderRadius: "5px",
        color: "#FFF",
        width: "50%",
        height: "500px",
        boxSizing:'border-box'
      }}
    >
      <div style={{padding:20}}>Drag Here({boxNumber})</div>
      {children}
    </div>
  );
};

export default Box;
