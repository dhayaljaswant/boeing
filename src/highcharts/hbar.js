import React from "react";
import { render } from "react-dom";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

const options1 = {
  title: {
    text: "My chart",
  },
  series: [
    {
      data: [1, 2],
    },
  ],
};

const options = {
  chart: {
    type: "bar",
  },
  title: {
    text: "Historic World Population by Region",
  },
  subtitle: {
    text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>',
  },
  xAxis: {
    categories: ["Africa", "America", "Asia", "Europe", "Oceania"],
    title: {
      text: null,
    },
  },
  yAxis: {
    min: 0,
    title: {
      text: "Population (millions)",
      align: "high",
    },
    labels: {
      overflow: "justify",
    },
  },
  tooltip: {
    valueSuffix: " millions",
  },
  plotOptions: {
    bar: {
      dataLabels: {
        enabled: true,
      },
    },
  },
  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "top",
    x: -40,
    y: 80,
    floating: true,
    borderWidth: 1,
    backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || "#FFFFFF",
    shadow: true,
  },
  credits: {
    enabled: false,
  },
  series: [
    {
      name: "Year 1800",
      data: [107, 31, 635, 203, 2],
    },
    {
      name: "Year 1900",
      data: [133, 156, 947, 408, 6],
    },

    // {
    //   name: 'Year 2000',
    //   data: [814, 841, 3714, 727, 31]
    // }, {
    //   name: 'Year 2016',
    //   data: [1216, 1001, 4436, 738, 40]
    // }
  ],
};

const option2 = {
  chart: {
    type: "column",
  },
  title: {
    text: "Stacked column chart",
  },
  xAxis: {
    categories: ["Apples", "Oranges", "Pears", "Grapes", "Bananas"],
  },
  yAxis: {
    min: 0,
    title: {
      text: "Total fruit consumption",
    },
  },
  tooltip: {
    pointFormat:
      '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
    shared: true,
  },
  plotOptions: {
    column: {
      stacking: "percent",
    },
  },
  series: [
    {
      name: "John",
      data: [5, 3, 4, 7, 2],
    },
    {
      name: "Jane",
      data: [2, 2, 3, 2, 1],
    },
    {
      name: "Joe",
      data: [3, 4, 4, 2, 5],
    },
  ],
};

const option3 = {
  title: {
    text: "Chart.update",
  },
  subtitle: {
    text: "Plain",
  },
  xAxis: {
    categories: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ],
  },
  series: [
    {
      type: "column",
      colorByPoint: true,
      data: [
        29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1,
        95.6, 54.4,
      ],
      showInLegend: false,
    },
  ],
};

const option4 = {
  chart: {
    type: "spline",
    scrollablePlotArea: {
      minWidth: 600,
      scrollPositionX: 1,
    },
  },
  title: {
    text: "Wind speed during two days",
    align: "left",
  },
  subtitle: {
    text: "13th & 14th of February, 2018 at two locations in Vik i Sogn, Norway",
    align: "left",
  },
  xAxis: {
    type: "datetime",
    labels: {
      overflow: "justify",
    },
  },
  yAxis: {
    title: {
      text: "Wind speed (m/s)",
    },
    minorGridLineWidth: 0,
    gridLineWidth: 0,
    alternateGridColor: null,
    plotBands: [
      {
        // Light air
        from: 0.3,
        to: 1.5,
        color: "rgba(68, 170, 213, 0.1)",
        label: {
          text: "Light air",
          style: {
            color: "#606060",
          },
        },
      },
      {
        // Light breeze
        from: 1.5,
        to: 3.3,
        color: "rgba(0, 0, 0, 0)",
        label: {
          text: "Light breeze",
          style: {
            color: "#606060",
          },
        },
      },
      {
        // Gentle breeze
        from: 3.3,
        to: 5.5,
        color: "rgba(68, 170, 213, 0.1)",
        label: {
          text: "Gentle breeze",
          style: {
            color: "#606060",
          },
        },
      },
      {
        // Moderate breeze
        from: 5.5,
        to: 8,
        color: "rgba(0, 0, 0, 0)",
        label: {
          text: "Moderate breeze",
          style: {
            color: "#606060",
          },
        },
      },
      {
        // Fresh breeze
        from: 8,
        to: 11,
        color: "rgba(68, 170, 213, 0.1)",
        label: {
          text: "Fresh breeze",
          style: {
            color: "#606060",
          },
        },
      },
      {
        // Strong breeze
        from: 11,
        to: 14,
        color: "rgba(0, 0, 0, 0)",
        label: {
          text: "Strong breeze",
          style: {
            color: "#606060",
          },
        },
      },
      {
        // High wind
        from: 14,
        to: 15,
        color: "rgba(68, 170, 213, 0.1)",
        label: {
          text: "High wind",
          style: {
            color: "#606060",
          },
        },
      },
    ],
  },
  tooltip: {
    valueSuffix: " m/s",
  },
  plotOptions: {
    spline: {
      lineWidth: 4,
      states: {
        hover: {
          lineWidth: 5,
        },
      },
      marker: {
        enabled: false,
      },
      pointInterval: 3600000, // one hour
      pointStart: Date.UTC(2018, 1, 13, 0, 0, 0),
    },
  },
  series: [
    {
      name: "Hestavollane",
      data: [
        3.7, 3.3, 3.9, 5.1, 3.5, 3.8, 4.0, 5.0, 6.1, 3.7, 3.3, 6.4, 6.9, 6.0,
        6.8, 4.4, 4.0, 3.8, 5.0, 4.9, 9.2, 9.6, 9.5, 6.3, 9.5, 10.8, 14.0, 11.5,
        10.0, 10.2, 10.3, 9.4, 8.9, 10.6, 10.5, 11.1, 10.4, 10.7, 11.3, 10.2,
        9.6, 10.2, 11.1, 10.8, 13.0, 12.5, 12.5, 11.3, 10.1,
      ],
    },
    {
      name: "Vik",
      data: [
        0.2, 0.1, 0.1, 0.1, 0.3, 0.2, 0.3, 0.1, 0.7, 0.3, 0.2, 0.2, 0.3, 0.1,
        0.3, 0.4, 0.3, 0.2, 0.3, 0.2, 0.4, 0.0, 0.9, 0.3, 0.7, 1.1, 1.8, 1.2,
        1.4, 1.2, 0.9, 0.8, 0.9, 0.2, 0.4, 1.2, 0.3, 2.3, 1.0, 0.7, 1.0, 0.8,
        2.0, 1.2, 1.4, 3.7, 2.1, 2.0, 1.5,
      ],
    },
  ],
  navigation: {
    menuItemStyle: {
      fontSize: "10px",
    },
  },
};

const HBar = ({count}) => (
  <div>
    {(!count || count === 1) && (
      <HighchartsReact highcharts={Highcharts} options={options} />
    )}

    {(!count || count === 2) && (
      <HighchartsReact highcharts={Highcharts} options={option2} />
    )}
    {(!count || count === 3) && (
      <HighchartsReact highcharts={Highcharts} options={option3} />
    )}
    {(!count || count === 4) && (
      <HighchartsReact highcharts={Highcharts} options={option4} />
    )}
  </div>
);

export default HBar;
