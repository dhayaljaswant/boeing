import React from "react";

// Pond
import { TimeSeries, Index } from "pondjs";

import { Charts, ChartContainer, ChartRow, YAxis, LineChart, styler, Baseline, Legend, Resizable, BarChart } from "react-timeseries-charts";
/// Ignore these next two lines
import barchart_docs from "./barchart_docs.md";
import barchart_thumbnail from "./barchart_thumbnail.png";
///

const data = [
    ["2017-01-24T00:00", 1000],
    ["2017-01-24T01:00", 200],
    ["2017-01-24T02:00", 300],
    ["2017-01-24T03:00", 240],

];

const series = new TimeSeries({
    name: "hilo_rainfall",
    columns: ["index", "precip"],
    points: data.map(([d, value]) => [Index.getIndexString("1h", new Date(d)), value])
});

console.log("series => ",series)

class barchart extends React.Component {
    static displayName = "BarChartExample";

    render() {
        const style = styler([{ key: "precip", color: "#A5C8E1", selected: "#2CB1CF" }]);

        return (
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <b>BarChart</b>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-md-12">
                        <Resizable>
                            <ChartContainer timeRange={series.range()}>
                                <ChartRow height="150" title="Rainfall sample">
                                    <YAxis
                                        id="rain"
                                        label="Rainfall (inches/hr)"
                                        min={0}
                                        max={1.5}
                                        format=".2f"
                                        width="70"
                                        type="linear"
                                    />
                                    <Charts>
                                        <BarChart
                                            axis="rain"
                                            style={style}
                                            spacing={1}
                                            columns={["precip"]}
                                            series={series}
                                            minBarHeight={1}
                                        />
                                    </Charts>
                                </ChartRow>
                            </ChartContainer>
                        </Resizable>
                    </div>
                </div>
            </div>
        );
    }
}

// Export example
export default barchart
