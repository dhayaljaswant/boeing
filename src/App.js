import logo from "./logo.svg";
import "./App.css";
import CurrencyNew from "../src/reliability/IndexNew";
import Currency from "../src/reliability/Index";
import Currency1 from "../src/reliability/Index_single";
// import BarChart from "../src/barchart/Index";
import HBar from "./highcharts/hbar";
import App1 from "./highcharts/reorder/App";

function App() {
  return (
    <div className="App1">
      <div>
      <App1/>
      </div>
      <HBar/>
      <Currency></Currency>
      <CurrencyNew></CurrencyNew>
      <Currency1></Currency1>
      {/* <BarChart></BarChart> */}
    </div>
  );
}

export default App;
